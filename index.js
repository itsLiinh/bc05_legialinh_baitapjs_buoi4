/** BÀI 1
 *
 * Input: Nhập 3 số nguyên
 *
 * Step:
 * Tạo biến lần lượt cho 3 số nguyên a,b,c
 * Tạo biến min, max để so sánh trung gian
 * Step 2:
 * b1:
 * So sánh 2 số a,b
 * Nếu a>b => a=max, b=min;
 * Nếu b>a => b=max, a=min;
 * b2:
 * So sánh c với max, min:
 * Nếu c>max => c=max;
 * Nếu C<min =>c=min;
 *
 * Output: 3 số nguyên theo thứ tự tăng dần
 */

function sapXep() {
  console.log("yes");

  var a = document.getElementById("txt-so-1").value * 1;
  var b = document.getElementById("txt-so-2").value * 1;
  var c = document.getElementById("txt-so-3").value * 1;

  var max = 0;
  var min = 0;

  if (a < b) {
    min = a;
    max = b;
  } else {
    min = b;
    max = a;
  }

  if (c > max) {
    max = c;
  } else if (c > min) {
    min = c;
  }

  document.getElementById("result").innerHTML =
    min + "," + (a + b + c - max - min) + "," + max;
}

/** BÀI 2
 *
 * Input: Lời chào
 *
 * step: Tạo biến cho option
 * Gọi B,M,A,E lần lượt là bố, mẹ, anh trai và em gái
 * gọi biến hello là kết quả của quá trình thiết lập
 *
 * Thiết lập câu chào:
 * Nếu là bố máy sẽ xuất ra câu  "Chào bố!";
 * Nếu là mẹ máy sẽ xuất ra câu  "Chào mẹ!";
 * Nếu là anh máy sẽ xuất ra câu  "Hello anh!"
 * Nếu là em máy sẽ xuất ra câu  "Hello em!"
 *
 * Output: show lời chào trên màn hình
 */

function guiLoiChao() {
  console.log("yes");

  var name = document.getElementById("member").value;
  var hello = document.getElementById("hello");

  if (name == "B") {
    hello.innerHTML = "Xin chào Bố!";
  } else if (name == "M") {
    hello.innerHTML = "Xin chào Mẹ!";
  } else if (name == "A") {
    hello.innerHTML = "Xin chào Anh trai!";
  } else if (name == "E") {
    hello.innerHTML = "Xin chào Em gái!";
  }
}

/** BÀI 3
 *
 * Input:
 * Gọi biến cho 3 số nguyên lần lượt là x,y,z
 * Gọi thêm biến count thể hiện cho số chẵn
 *
 * Step:
 * Cách nhận biết chẵn lẻ:
 * Đem số nguyên chia % cho 2 .Nếu kết qủa = 0 thì chia hết, tăng count lên 1 (count++)
 * Ngược lại không cộng => số lẻ
 *count là số lượng số chẵn, số lượng số lẻ lấy 3-count.
 *
 * Output:
 *  Xuất thông tin ra cho ng dùng:
 *
 *
 *
 */

function demSo() {
  console.log("yes");
  var x = document.getElementById("txt-num-1").value * 1;
  var y = document.getElementById("txt-num-2").value * 1;
  var z = document.getElementById("txt-num-3").value * 1;

  var count = 0;

  if (x % 2 == 0) {
    count++;
  }
  if (y % 2 == 0) {
    count++;
  }
  if (z % 2 == 0) {
    count++;
  }

  document.getElementById("countNum").innerHTML =
    "Tổng số chẳn=" + count + ", Tổng số lẻ" + (3 - count);
}

/** BÀI 4
 *
 * Input:
 *  Gọi 3 biến là 3 cạnh tam giác lần lượt là a,b,c:
 * Gọi biến result là kết quả thông tin
 *
 * Step:
 * Sử dụng hàm If-else để phân loại tam giác:
 * Xét điều kiện 3 cạnh của 1 tam giác bằng cách tính tổng của 2 cạnh phải lớn hơn cạnh còn lại
 * Đặt điều kiện:
 * -Nếu a==b || a==c ||b==c =>tam giác cân
 * -Nếu a==b && b==c => tam giác đều
 * -Nếu c = Math.sqrt(a*a +b*b)=>tam giác vuông
 * -Nếu không đúng 3 trường hợp trên =>tam giác thường
 *
 * Output: Xuất thông tin trên màn hình
 */

function duDoan() {
  console.log("yes");

  var canh1 = document.getElementById("txt-canh-1").value * 1;
  var canh2 = document.getElementById("txt-canh-2").value * 1;
  var canh3 = document.getElementById("txt-canh-3").value * 1;

  if (canh1 + canh2 > canh3 && canh1 + canh3 > canh2 && canh2 + canh3 > canh1) {
    if (canh1 == canh2 && canh1 == canh3) {
      document.getElementById("thongBao").innerHTML = "Tam giác đều";
    } else if (canh1 == canh2 || canh1 == canh3 || canh2 == canh3) {
      document.getElementById("thongBao").innerHTML = "Tam giác cân";
    } else if (
      canh3 * canh3 == canh1 * canh1 + canh2 * canh2 ||
      canh2 * canh2 == canh1 * canh1 + canh3 * canh3 ||
      canh1 * canh1 == canh2 * canh2 + canh3 * canh3
    ) {
      document.getElementById("thongBao").innerHTML = "Tam giác vuông";
    } else {
      document.getElementById("thongBao").innerHTML = "Tam giác thường";
    }
  }
}

/*Bài 4 thêm tính ngày hôm qua vs ngày mai*/

// Hàm kiểm tra xem ngày trước đó là ngày nào.
function homQua() {
  var d = document.getElementById("txt-ngay").value * 1;
  var m = document.getElementById("txt-thang").value * 1;
  var y = document.getElementById("txt-nam").value * 1;

  var namNhuan = function (y) {
    return (y % 4 == 0 && y % 100 !== 0) || y % 400 == 0;
  };

  var soNgayTrongThang = function (m) {
    switch (m) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        return 31;
      case 2: {
        if (namNhuan(y) == true) {
          return 29;
        }
        return 28;
      }
      case 4:
      case 6:
      case 9:
      case 11:
        return 30;
    }

    return alert("error");
  };

  var kiemTraDayMonth = function () {
    return y > 0 && m > 0 && m < 13 && d > 0 && d <= soNgayTrongThang(m);
  };

  var nd = d;
  var nm = m;
  var ny = y;

  if (kiemTraDayMonth() == true) {
    nd = d - 1;
    if (m !== 1 && d == 1) {
      nm = m - 1;
      nd = soNgayTrongThang(nm);
    } else if (m == 1 && d == 1) {
      nd = 30;
      nm = 12;
      ny = y - 1;
    }
  } else {
    return alert("error");
  }

  return (document.getElementById(
    "ngayHomQua"
  ).innerHTML = ` ${nd}/${nm}/${ny}`);
}

// Hàm kiểm tra xem ngày tiếp theo là ngày nào.
function ngayMai() {
  var d = document.getElementById("txt-ngay").value * 1;
  var m = document.getElementById("txt-thang").value * 1;
  var y = document.getElementById("txt-nam").value * 1;

  var namNhuan = function (y) {
    return (y % 4 == 0 && y % 100 !== 0) || y % 400 == 0;
  };

  var soNgayTrongThang = function (m) {
    switch (m) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        return 31;
      case 2: {
        if (namNhuan(y) == true) {
          return 29;
        }
        return 28;
      }
      case 4:
      case 6:
      case 9:
      case 11:
        return 30;
    }
    return alert("error");
  };

  var kiemTraDayMonth = function () {
    return y > 0 && m > 0 && m < 13 && d > 0 && d <= soNgayTrongThang(m);
  };

  var xd = d;
  var xm = m;
  var xy = y;

  if (kiemTraDayMonth() == true) {
    xd = d + 1;
    if (m !== 12 && d == soNgayTrongThang(m)) {
      xd = 1;
      xm = m + 1;
    } else if ((m = 12 && d == soNgayTrongThang(m))) {
      xd = 1;
      xm = 1;
      xy = y + 1;
    } else if (m == 2) {
      switch (namNhuan(y)) {
        case 29:
        case 28:
          xd = 1;
          xm = m + 1;
          break;
      }
    }
  } else {
    return alert("error");
  }

  return (document.getElementById(
    "ngayTiepTheo"
  ).innerHTML = ` ${xd}/${xm}/${xy}`);
}

/**Bài 4 thêm tính ngày */

function tinhNgay() {

  var m = document.getElementById("txt-thang").value * 1;
  var y = document.getElementById("txt-nam").value * 1;

  var namNhuan = function () {
    return (y % 4 == 0 && y % 100 !== 0) || y % 400 == 0;
  };

  if (y <= 0) {
    return alert("Lỗi");
  }

  var soNgayTrongThang = function () {
    switch (m) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        return 31;
      case 2:
        if (namNhuan()) {
          return 29;
        } else {
          return 28;
        }
      case 4:
      case 6:
      case 9:
      case 10:
      case 11:
        return 30;
    }
    return alert("Lỗi");
  };

  return (document.getElementById(
    "ketQuaLa"
  ).innerHTML = `${soNgayTrongThang()} "ngày"`);
}
